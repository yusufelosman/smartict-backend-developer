package com.yusufelosman.smartict.execption;

public class BusinessException extends Exception {
    public BusinessException(String message) {
        super(message);
    }
}
