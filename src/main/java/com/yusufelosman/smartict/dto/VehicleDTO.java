package com.yusufelosman.smartict.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
public class VehicleDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private long id;

    @NotBlank
    @Size(min = 3, max = 100)
    private String name;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private RouteDTO routeDTO;
}
