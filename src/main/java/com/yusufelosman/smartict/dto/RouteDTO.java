package com.yusufelosman.smartict.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
public class RouteDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private long id;

    @NotBlank
    @Size(min = 3, max = 100)
    private String name;

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
