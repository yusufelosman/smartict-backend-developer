package com.yusufelosman.smartict.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AssignRouteToVehicleDTO {
    @NotNull
    private Long vehicleID;

    @NotNull
    private Long routeID;
}
