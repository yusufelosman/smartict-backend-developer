package com.yusufelosman.smartict.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AddStationToRouteDTO {

    @NotNull
    private Long routeID;

    @NotNull
    private Long stationID;
}
