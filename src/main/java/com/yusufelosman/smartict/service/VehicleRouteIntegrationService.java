package com.yusufelosman.smartict.service;

import com.yusufelosman.smartict.entity.RouteEntity;
import com.yusufelosman.smartict.entity.VehicleEntity;
import com.yusufelosman.smartict.execption.BusinessException;
import com.yusufelosman.smartict.repository.RouteRepository;
import com.yusufelosman.smartict.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;

@Service
public class VehicleRouteIntegrationService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private RouteRepository routeRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void assignRouteToVehicle(Long vehicleID, Long routeID) throws BusinessException {
        RouteEntity routeEntity = routeRepository.findById(routeID)
                .orElseThrow(() -> new EntityNotFoundException("Route " + routeID));

        if (routeEntity.getStations().size() < 2) {
            throw new BusinessException("A route should consist of at least 2 stations");
        }
        VehicleEntity vehicleEntity = vehicleRepository.findById(vehicleID)
                .orElseThrow(() -> new EntityNotFoundException("Vehicle " + vehicleID));


        Query query = entityManager.createNativeQuery("select r.route_id from route_vehicle r where r.vehicle_id = :vehicle_id")
                .setParameter("vehicle_id", vehicleID);
        List<BigInteger> resultList = query.getResultList();

        if (resultList.size() > 0) {
            RouteEntity routeEntity1 = routeRepository.findById(resultList.get(0).longValue()).get();
            routeEntity1.getVehicles().remove(vehicleEntity);
            routeRepository.saveAndFlush(routeEntity1);
        }

        if (routeEntity.getVehicles().contains(vehicleEntity)) {
            routeEntity.getVehicles().remove(vehicleEntity);
            routeRepository.saveAndFlush(routeEntity);
        }
        routeEntity.getVehicles().add(vehicleEntity);
        routeRepository.saveAndFlush(routeEntity);

        vehicleEntity.setRoute(null);
        vehicleRepository.saveAndFlush(vehicleEntity);

        vehicleEntity.setRoute(routeEntity);
        vehicleRepository.saveAndFlush(vehicleEntity);
    }
}
