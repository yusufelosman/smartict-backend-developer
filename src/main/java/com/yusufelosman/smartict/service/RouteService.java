package com.yusufelosman.smartict.service;

import com.yusufelosman.smartict.dto.RouteDTO;
import com.yusufelosman.smartict.entity.RouteEntity;
import com.yusufelosman.smartict.repository.RouteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static com.yusufelosman.smartict.util.EntityToDtoConverter.routeEntityRouteDTOFunction;

@Service
public class RouteService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RouteRepository routeRepository;

    @Transactional(readOnly = true)
    public List<RouteDTO> getAllRoute() {
        List<RouteEntity> routeEntityList = routeRepository.findAll();
        return routeEntityList.stream()
                .map(routeEntityRouteDTOFunction)
                .collect(Collectors.toList());
    }

    public void deleteRoute(long id) {
        if (!routeRepository.existsById(id)) {
            throw new EntityNotFoundException(String.valueOf(id));
        }
        routeRepository.deleteById(id);
    }

    public RouteDTO getRouteById(long id) {
        RouteEntity routeEntity = routeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
        return routeEntityRouteDTOFunction.apply(routeEntity);
    }

    public long addRoute(RouteDTO routeDTO) {
        RouteEntity routeEntity = modelMapper.map(routeDTO, RouteEntity.class);
        routeEntity = routeRepository.save(routeEntity);
        return routeEntity.getId();
    }

    public RouteDTO updateRoute(long id, RouteDTO routeDTO) {
        if (!routeRepository.existsById(id)) {
            throw new EntityNotFoundException(String.valueOf(id));
        }

        RouteEntity routeEntity = routeRepository.getById(id);
        routeEntity.setName(routeDTO.getName());
        routeRepository.saveAndFlush(routeEntity);

        return routeEntityRouteDTOFunction.apply(routeEntity);
    }

    public RouteDTO getRouteByName(String name) {
        RouteEntity routeEntity = routeRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException(name));
        return routeEntityRouteDTOFunction.apply(routeEntity);
    }
}
