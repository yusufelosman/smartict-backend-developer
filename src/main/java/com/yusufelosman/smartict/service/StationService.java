package com.yusufelosman.smartict.service;

import com.yusufelosman.smartict.dto.StationDTO;
import com.yusufelosman.smartict.entity.StationEntity;
import com.yusufelosman.smartict.repository.StationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static com.yusufelosman.smartict.util.EntityToDtoConverter.stationEntityStationDTOFunction;

@Service
public class StationService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private StationRepository stationRepository;

    public List<StationDTO> getAllStation() {
        List<StationEntity> stationEntityList = stationRepository.findAll();
        return stationEntityList.stream().map(stationEntityStationDTOFunction)
                .collect(Collectors.toList());
    }

    public void deleteStation(long id) {
        if (!stationRepository.existsById(id)) {
            throw new EntityNotFoundException(String.valueOf(id));
        }
        stationRepository.deleteById(id);
    }

    public StationDTO getStationById(long id) {
        StationEntity stationEntity = stationRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
        return stationEntityStationDTOFunction.apply(stationEntity);
    }

    public long addStation(StationDTO stationDTO) {
        StationEntity stationEntity = modelMapper.map(stationDTO, StationEntity.class);
        stationEntity = stationRepository.save(stationEntity);
        return stationEntity.getId();
    }

    public StationDTO updateStation(long id, StationDTO stationDTO) {
        if (!stationRepository.existsById(id)) {
            throw new EntityNotFoundException(String.valueOf(id));
        }
        StationEntity stationEntity = stationRepository.getById(id);
        stationEntity.setName(stationDTO.getName());
        stationRepository.saveAndFlush(stationEntity);

        return stationEntityStationDTOFunction.apply(stationEntity);
    }

    public StationDTO getStationByName(String name) {
        StationEntity stationEntity = stationRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException(name));
        return stationEntityStationDTOFunction.apply(stationEntity);
    }
}
