package com.yusufelosman.smartict.service;

import com.yusufelosman.smartict.dto.RouteDTO;
import com.yusufelosman.smartict.dto.StationDTO;
import com.yusufelosman.smartict.entity.RouteEntity;
import com.yusufelosman.smartict.entity.StationEntity;
import com.yusufelosman.smartict.repository.RouteRepository;
import com.yusufelosman.smartict.repository.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static com.yusufelosman.smartict.util.EntityToDtoConverter.routeEntityRouteDTOFunction;
import static com.yusufelosman.smartict.util.EntityToDtoConverter.stationEntityStationDTOFunction;

@Service
public class StationRouteIntegrationService {

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private StationRepository stationRepository;

    public void addStationToRoute(Long routeID, Long stationId) {
        RouteEntity routeEntity = routeRepository.findById(routeID)
                .orElseThrow(() -> new EntityNotFoundException("Route " + routeID));

        StationEntity stationEntity = stationRepository.findById(stationId)
                .orElseThrow(() -> new EntityNotFoundException("Station " + routeID));

        routeEntity.getStations().add(stationEntity);
        stationEntity.getRoutes().add(routeEntity);

        routeRepository.save(routeEntity);
        stationRepository.save(stationEntity);
    }

    public List<StationDTO> getStationOfRoute(long routeId) {
        RouteEntity routeEntity = routeRepository.findById(routeId)
                .orElseThrow(() -> new EntityNotFoundException("Route " + routeId));

        return routeEntity.getStations()
                .stream()
                .map(stationEntityStationDTOFunction)
                .collect(Collectors.toList());
    }

    public List<RouteDTO> getRouteIncludeStation(long stationId) {
        StationEntity stationEntity = stationRepository.findById(stationId)
                .orElseThrow(() -> new EntityNotFoundException("Station " + stationId));

        return stationEntity.getRoutes()
                .stream()
                .map(routeEntityRouteDTOFunction)
                .collect(Collectors.toList());
    }
}
