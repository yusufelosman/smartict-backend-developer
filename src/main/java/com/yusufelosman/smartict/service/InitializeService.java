package com.yusufelosman.smartict.service;

import com.yusufelosman.smartict.entity.RouteEntity;
import com.yusufelosman.smartict.entity.StationEntity;
import com.yusufelosman.smartict.entity.VehicleEntity;
import com.yusufelosman.smartict.repository.RouteRepository;
import com.yusufelosman.smartict.repository.StationRepository;
import com.yusufelosman.smartict.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class InitializeService {

    @Autowired
    private JdbcUserDetailsManager jdbcUserDetailsManager;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private StationRepository stationRepository;

    @EventListener(ApplicationReadyEvent.class)
    private void init() {
        initUser();
        initData();

        System.err.println("Application Started With Test Data");
    }

    private void initData() {
        VehicleEntity vehicle1 = VehicleEntity.builder().name("vehicle1").build();
        vehicle1 = vehicleRepository.saveAndFlush(vehicle1);

        StationEntity station1 = StationEntity.builder().name("station1").build();
        StationEntity station2 = StationEntity.builder().name("station2").build();
        StationEntity station3 = StationEntity.builder().name("station3").build();
        station1 = stationRepository.saveAndFlush(station1);
        station2 = stationRepository.saveAndFlush(station2);
        station3 = stationRepository.saveAndFlush(station3);

        RouteEntity route1 = RouteEntity.builder().name("route1").build();
        route1 = routeRepository.saveAndFlush(route1);

        Set<StationEntity> stations = new HashSet<>();
        stations.add(station1);
        stations.add(station2);
        stations.add(station3);
        route1.setStations(stations);

        station1.getRoutes().add(route1);
        station2.getRoutes().add(route1);
        station3.getRoutes().add(route1);

        vehicle1.setRoute(route1);
        route1.getVehicles().add(vehicle1);

        stationRepository.saveAndFlush(station1);
        stationRepository.saveAndFlush(station2);
        stationRepository.saveAndFlush(station3);
        routeRepository.saveAndFlush(route1);
        vehicleRepository.saveAndFlush(vehicle1);
    }

    private void initUser() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        User user = new User("sa", encoder.encode("sa"), authorities);
        jdbcUserDetailsManager.createUser(user);
    }
}
