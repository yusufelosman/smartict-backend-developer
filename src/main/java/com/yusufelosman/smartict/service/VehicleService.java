package com.yusufelosman.smartict.service;

import com.yusufelosman.smartict.dto.VehicleDTO;
import com.yusufelosman.smartict.entity.VehicleEntity;
import com.yusufelosman.smartict.repository.VehicleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static com.yusufelosman.smartict.util.EntityToDtoConverter.vehicleEntityVehicleDTOFunction;

@Service
public class VehicleService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private VehicleRepository vehicleRepository;

    public List<VehicleDTO> getAllVehicle() {
        List<VehicleEntity> vehicleEntityList = vehicleRepository.findAll();
        return vehicleEntityList.stream().map(vehicleEntityVehicleDTOFunction)
                .collect(Collectors.toList());
    }

    public void deleteVehicle(long id) {
        if (!vehicleRepository.existsById(id)) {
            throw new EntityNotFoundException(String.valueOf(id));
        }
        vehicleRepository.deleteById(id);
    }

    public VehicleDTO getVehicleById(long id) {
        VehicleEntity vehicleEntity = vehicleRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
        return vehicleEntityVehicleDTOFunction.apply(vehicleEntity);
    }

    public long addVehicle(VehicleDTO vehicleDTO) {
        VehicleEntity vehicleEntity = modelMapper.map(vehicleDTO, VehicleEntity.class);
        vehicleEntity = vehicleRepository.save(vehicleEntity);
        return vehicleEntity.getId();
    }

    public VehicleDTO updateVehicle(long id, VehicleDTO vehicleDTO) {
        if (!vehicleRepository.existsById(id)) {
            throw new EntityNotFoundException(String.valueOf(id));
        }
        VehicleEntity vehicleEntity = vehicleRepository.getById(id);
        vehicleEntity.setName(vehicleDTO.getName());
        vehicleRepository.saveAndFlush(vehicleEntity);

        return vehicleEntityVehicleDTOFunction.apply(vehicleEntity);
    }

    public VehicleDTO getVehicleByName(String name) {
        VehicleEntity vehicleEntity = vehicleRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException(name));
        return vehicleEntityVehicleDTOFunction.apply(vehicleEntity);
    }
}
