package com.yusufelosman.smartict.util;

import com.yusufelosman.smartict.dto.RouteDTO;
import com.yusufelosman.smartict.dto.StationDTO;
import com.yusufelosman.smartict.dto.VehicleDTO;
import com.yusufelosman.smartict.entity.RouteEntity;
import com.yusufelosman.smartict.entity.StationEntity;
import com.yusufelosman.smartict.entity.VehicleEntity;

import java.util.function.Function;

public class EntityToDtoConverter {
    public static Function<RouteEntity, RouteDTO> routeEntityRouteDTOFunction = routeEntity -> RouteDTO.builder()
            .id(routeEntity.getId())
            .name(routeEntity.getName())
            .build();

    public static Function<StationEntity, StationDTO> stationEntityStationDTOFunction = routeEntity -> StationDTO.builder()
            .id(routeEntity.getId())
            .name(routeEntity.getName())
            .build();

    public static Function<VehicleEntity, VehicleDTO> vehicleEntityVehicleDTOFunction = vehicleEntity -> VehicleDTO.builder()
            .id(vehicleEntity.getId())
            .name(vehicleEntity.getName())
            .routeDTO(vehicleEntity.getRoute() != null ? RouteDTO.builder()
                    .id(vehicleEntity.getRoute().getId())
                    .name(vehicleEntity.getRoute().getName())
                    .build() : null)
            .build();
}
