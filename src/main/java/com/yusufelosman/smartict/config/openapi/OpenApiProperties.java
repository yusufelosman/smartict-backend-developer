package com.yusufelosman.smartict.config.openapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "openapi")
@ConstructorBinding
@AllArgsConstructor
@Getter
public class OpenApiProperties {

    private final String projectTitle = "Yusuf Elosman";
    private final String projectDescription = "SmartICT";
    private final String projectVersion = "1.0.0";
}