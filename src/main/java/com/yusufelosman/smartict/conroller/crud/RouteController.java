package com.yusufelosman.smartict.conroller.crud;

import com.yusufelosman.smartict.dto.RouteDTO;
import com.yusufelosman.smartict.service.RouteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/route/")
@Tag(name = "Route CRUD Operation")
public class RouteController {

    @Autowired
    private RouteService routeService;

    @Operation(summary = "Create New Route")
    @PostMapping("/createRoute")
    private ResponseEntity<?> create(@Valid @RequestBody RouteDTO routeDTO) {
        long id = routeService.addRoute(routeDTO);
        return ResponseEntity.ok("Route Added, Id = " + id);
    }

    @Operation(summary = "Update Route Name By Id")
    @PutMapping("/updateRouteName/{routeId}")
    private ResponseEntity<?> update(@PathVariable(value = "routeId") long id, @Valid @RequestBody RouteDTO routeDTO) {
        routeDTO = routeService.updateRoute(id, routeDTO);
        return ResponseEntity.ok(routeDTO.toString());
    }

    @Operation(summary = "Get Route By Id")
    @GetMapping("/{routeId}")
    private ResponseEntity<?> getRoute(@PathVariable("routeId") long id) {
        RouteDTO routeDTO = routeService.getRouteById(id);
        return ResponseEntity.ok(routeDTO);
    }

    @Operation(summary = "Get Route By Name")
    @GetMapping("/getRouteByName/{routeName}")
    private ResponseEntity<?> getRouteByName(@PathVariable("routeName") String name) {
        RouteDTO routeDTO = routeService.getRouteByName(name);
        return ResponseEntity.ok(routeDTO);
    }

    @Operation(summary = "Delete Route By Id")
    @DeleteMapping("/{routeId}")
    public ResponseEntity<?> deleteRoute(@PathVariable("routeId") long id) {
        routeService.deleteRoute(id);
        return ResponseEntity.ok("Route deleted : " + id);
    }

    @Operation(summary = "Get All Route")
    @GetMapping("/getAll")
    public ResponseEntity<?> getAllRoute() {
        List<RouteDTO> routes = routeService.getAllRoute();
        return ResponseEntity.ok(routes);
    }
}
