package com.yusufelosman.smartict.conroller.crud;

import com.yusufelosman.smartict.dto.StationDTO;
import com.yusufelosman.smartict.service.StationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/station/")
@Tag(name = "Station CRUD Operation")
public class StationController {

    @Autowired
    private StationService stationService;

    @Operation(summary = "Create New Station")
    @PostMapping("/createStation")
    private ResponseEntity<?> create(@Valid @RequestBody StationDTO stationDTO) {
        long id = stationService.addStation(stationDTO);
        return ResponseEntity.ok("Station Added, Id = " + id);
    }

    @Operation(summary = "Update Station Name By Id")
    @PutMapping("/updateStationName/{stationId}")
    private ResponseEntity<?> update(@PathVariable("stationId") long id, @Valid @RequestBody StationDTO stationDTO) {
        stationDTO = stationService.updateStation(id, stationDTO);
        return ResponseEntity.ok(stationDTO.toString());
    }

    @Operation(summary = "Get Station By Name")
    @GetMapping("/getStationByName/{stationName}")
    private ResponseEntity<?> getStationByName(@PathVariable("stationName") String name) {
        StationDTO stationDTO = stationService.getStationByName(name);
        return ResponseEntity.ok(stationDTO);
    }

    @Operation(summary = "Get Station By Id")
    @GetMapping("/{stationId}")
    private ResponseEntity<?> getStation(@PathVariable("stationId") long id) {
        StationDTO stationDTO = stationService.getStationById(id);
        return ResponseEntity.ok(stationDTO);
    }

    @Operation(summary = "Delete Station By Id")
    @DeleteMapping("/{stationId}")
    public ResponseEntity<?> getAllStation(@PathVariable("stationId") long id) {
        stationService.deleteStation(id);
        return ResponseEntity.ok("Station deleted : " + id);
    }

    @Operation(summary = "Get All Station")
    @GetMapping("/getAll")
    public ResponseEntity<?> getAllStation() {
        List<StationDTO> stations = stationService.getAllStation();
        return ResponseEntity.ok(stations);
    }
}
