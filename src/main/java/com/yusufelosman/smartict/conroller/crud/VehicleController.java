package com.yusufelosman.smartict.conroller.crud;

import com.yusufelosman.smartict.dto.VehicleDTO;
import com.yusufelosman.smartict.service.VehicleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/vehicle/")
@Tag(name = "Vehicle CRUD Operation")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @Operation(summary = "Create New Vehicle")
    @PostMapping("/createVehicle")
    private ResponseEntity<?> create(@Valid @RequestBody VehicleDTO vehicleDTO) {
        long id = vehicleService.addVehicle(vehicleDTO);
        return ResponseEntity.ok("Vehicle Added, Id = " + id);
    }

    @Operation(summary = "Update Vehicle Name By Id")
    @PutMapping("/updateVehicleName/{vehicleId}")
    private ResponseEntity<?> update(@PathVariable("vehicleId") long id, @Valid @RequestBody VehicleDTO vehicleDTO) {
        vehicleDTO = vehicleService.updateVehicle(id, vehicleDTO);
        return ResponseEntity.ok(vehicleDTO.toString());
    }

    @Operation(summary = "Get Vehicle By Name")
    @GetMapping("/getVehicleByName/{vehicleName}")
    private ResponseEntity<?> getVehicleByName(@PathVariable("vehicleName") String name) {
        VehicleDTO routeDTO = vehicleService.getVehicleByName(name);
        return ResponseEntity.ok(routeDTO);
    }


    @Operation(summary = "Get Vehicle By Id")
    @GetMapping("/{vehicleId}")
    private ResponseEntity<?> getVehicle(@PathVariable("vehicleId") long id) {
        VehicleDTO vehicleDTO = vehicleService.getVehicleById(id);
        return ResponseEntity.ok(vehicleDTO);
    }

    @Operation(summary = "Delete Vehicle By Id")
    @DeleteMapping("/{vehicleId}")
    public ResponseEntity<?> getAllVehicle(@PathVariable("vehicleId") long id) {
        vehicleService.deleteVehicle(id);
        return ResponseEntity.ok("Vehicle deleted : " + id);
    }

    @Operation(summary = "Get All Vehicle")
    @GetMapping("/getAll")
    public ResponseEntity<?> getAllVehicle() {
        List<VehicleDTO> vehicles = vehicleService.getAllVehicle();
        return ResponseEntity.ok(vehicles);
    }
}
