package com.yusufelosman.smartict.conroller.operation;

import com.yusufelosman.smartict.dto.AddStationToRouteDTO;
import com.yusufelosman.smartict.dto.RouteDTO;
import com.yusufelosman.smartict.dto.StationDTO;
import com.yusufelosman.smartict.service.StationRouteIntegrationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/operation/")
@Tag(name = "Station-Route Integration Operations")
public class StationRouteIntegrationController {

    @Autowired
    private StationRouteIntegrationService stationRouteIntegrationService;

    @Operation(summary = "Add Station To Route")
    @PostMapping("/addStationToRoute")
    private ResponseEntity<?> addStationToRoute(@Valid @RequestBody AddStationToRouteDTO addStationToRoute) {
        stationRouteIntegrationService.addStationToRoute(addStationToRoute.getRouteID(), addStationToRoute.getStationID());
        return ResponseEntity.ok("Station " + addStationToRoute.getStationID() + " Added to Route " + addStationToRoute.getRouteID());
    }


    @Operation(summary = "Get Station Of Route")
    @PostMapping("/getStationOfRoute/{routeId}")
    private ResponseEntity<?> getStationOfRoute(@PathVariable(value = "routeId") long id) {
        List<StationDTO> stations = stationRouteIntegrationService.getStationOfRoute(id);
        return ResponseEntity.ok(stations);
    }

    @Operation(summary = "Get Route Include Station")
    @PostMapping("/getRouteIncludeStation/{stationId}")
    private ResponseEntity<?> getRouteIncludeStation(@PathVariable(value = "stationId") long id) {
        List<RouteDTO> routes = stationRouteIntegrationService.getRouteIncludeStation(id);
        return ResponseEntity.ok(routes);
    }


}
