package com.yusufelosman.smartict.conroller.operation;

import com.yusufelosman.smartict.dto.AssignRouteToVehicleDTO;
import com.yusufelosman.smartict.execption.BusinessException;
import com.yusufelosman.smartict.service.VehicleRouteIntegrationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/operation/")
@Tag(name = "Vehicle-Route Integration Operations")
public class VehicleRouteIntegrationController {

    @Autowired
    private VehicleRouteIntegrationService vehicleRouteIntegrationService;

    @Operation(summary = "Assign Route To Vehicle")
    @PostMapping("/assignRouteToVehicle")
    private ResponseEntity<?> assignRouteToVehicle(@Valid @RequestBody AssignRouteToVehicleDTO assignRouteToVehicle) throws BusinessException {
        vehicleRouteIntegrationService.assignRouteToVehicle(assignRouteToVehicle.getVehicleID(), assignRouteToVehicle.getRouteID());
        return ResponseEntity.ok("Route " + assignRouteToVehicle.getRouteID() + " Assign To " + assignRouteToVehicle.getVehicleID());
    }
}
