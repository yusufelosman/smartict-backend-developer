package com.yusufelosman.smartict.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(unique = true)
    @Size(min = 3, max = 100)
    private String name;

    @Builder.Default
    @ManyToMany(mappedBy = "stations", fetch = FetchType.LAZY)
    private Set<RouteEntity> routes = new HashSet<>();
}
