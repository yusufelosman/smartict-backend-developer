package com.yusufelosman.smartict;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class SmartICTApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartICTApplication.class, args);
    }

}
