package com.yusufelosman.smartict;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.yusufelosman.smartict.dto.AssignRouteToVehicleDTO;
import com.yusufelosman.smartict.dto.RouteDTO;
import com.yusufelosman.smartict.dto.StationDTO;
import com.yusufelosman.smartict.dto.VehicleDTO;
import com.yusufelosman.smartict.entity.RouteEntity;
import com.yusufelosman.smartict.entity.VehicleEntity;
import com.yusufelosman.smartict.repository.RouteRepository;
import com.yusufelosman.smartict.repository.VehicleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class SmartICTApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private com.yusufelosman.smartict.service.VehicleRouteIntegrationService VehicleRouteIntegrationService;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Test
    void createVehicleTest() throws Exception {
        VehicleDTO vehicleDTO = VehicleDTO.builder()
                .name("vehicle1")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(vehicleDTO);

        this.mockMvc.perform(post("/vehicle/createVehicle")
                .with(httpBasic("sa", "sa"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void createRouteTest() throws Exception {
        RouteDTO routeDTO = RouteDTO.builder()
                .name("test route")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(routeDTO);

        this.mockMvc.perform(post("/route/createRoute")
                .with(httpBasic("sa", "sa"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void createStationTest() throws Exception {
        StationDTO stationDTO = StationDTO.builder()
                .name("test station")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(stationDTO);

        this.mockMvc.perform(post("/station/createStation")
                .with(httpBasic("sa", "sa"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void assignRouteToVehicleTest() throws Exception {
        VehicleDTO vehicleDTO = VehicleDTO.builder()
                .name("vehicle2")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(vehicleDTO);

        this.mockMvc.perform(post("/vehicle/createVehicle")
                .with(httpBasic("sa", "sa"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)).andDo(print()).andExpect(status().isOk());


        this.mockMvc.perform(get("/route/getRouteByName/{routeName}", "route1")
                .with(httpBasic("sa", "sa"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)).andDo(print()).andExpect(status().isOk());

        RouteEntity route2 = routeRepository.findByName("route1").get();
        VehicleEntity vehicle2 = vehicleRepository.findByName("vehicle2").get();

        AssignRouteToVehicleDTO assignRouteToVehicleDTO = new AssignRouteToVehicleDTO();
        assignRouteToVehicleDTO.setVehicleID(vehicle2.getId());
        assignRouteToVehicleDTO.setRouteID(route2.getId());

        mapper = new ObjectMapper();
        ow = mapper.writer().withDefaultPrettyPrinter();
        requestJson = ow.writeValueAsString(assignRouteToVehicleDTO);

        this.mockMvc.perform(post("/operation/assignRouteToVehicle")
                .with(httpBasic("sa", "sa"))
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson)).andDo(print()).andExpect(status().isOk());

    }
}
