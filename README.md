# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Veri Tabanı ###

* [Veri Tabanı adresi] (http://localhost:8080/h2-console)
* **H2**
* **Driver Class:** org.h2.Driver  
* **JDBC URL:** jdbc:h2:mem:testdb  
* **Kullanıcı Adı:** sa
* **Şifre:** sa


### Swagger UI ###

* [Swagger-UI adresi] (http://localhost:8080/swagger-ui)
* **Open Api**
* **Kullanıcı** Adı: sa
* **Şifre:** sa
